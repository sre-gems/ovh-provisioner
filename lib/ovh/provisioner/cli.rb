# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'
require 'pp'
require 'ruby-progressbar'
require 'highline/import'
Dir[File.dirname(__FILE__) + '/cli*.rb'].each { |file| require file }

# Monkey patch Thor to fix #261
module ThorPatching
  def banner(command, namespace = nil, subcommand = false)
    array = super.split(' ')
    array[1] = array[1].tr('_', '')[3..-1] if array[1].start_with?('cli_')
    array.join(' ')
  end
end

# Prepending the patch
class Thor
  class << self
    prepend ThorPatching
  end
end

module OVH
  module Provisioner
    # The command line runner
    class Cli < Thor # rubocop:disable Metrics/ClassLength
      desc 'version', 'print OVH Provisioner\'s version information'
      def version
        say "OVH Provisioner version #{OVH::Provisioner::VERSION}"
      end
      map %w[-v --version] => :version

      desc 'list targets', 'print the list of your dedicated servers'
      def list(*targets)
        spawner = Provisioner.init(options)
        puts spawner
          .get('DedicatedServer', *self.class.all(targets))
          .format('vrack', 'flavor')
      end

      option(
        :force,
        type: :boolean,
        desc: 'Force the installation even if the server is already installed',
        default: false,
        aliases: ['-f']
      )
      option(
        :template,
        desc: 'Template to install',
        aliases: ['-t']
      )
      option(
        :ssh_key_name,
        desc: 'Name of the ssh key to install',
        aliases: ['-n']
      )
      option(
        :use_distrib_kernel,
        type: :boolean,
        desc: 'Use official kernel instead of OVH\'s (default = false)',
        aliases: ['-d']
      )
      option(
        :custom_hostname,
        desc: 'Hostname of the server (default = reverse)',
        aliases: ['-h']
      )
      desc 'install targets', 'install/reinstall one or more dedicated servers'
      def install(*targets)
        spawner = Provisioner.init(options)
        servers = spawner.get('DedicatedServer', *self.class.all(targets))
        self.class.ask_validation(
          'You are going to (re)install those servers:',
          servers.format('install_status', 'flavor')
        )
        servers.puts_each(:install)
      end

      NAME_SCHEME = [
        :name_scheme,
        desc: 'Name scheme to use for the servers, ex: %<flavor_tag>s-%<id>s',
        aliases: ['-n']
      ].freeze
      NAME_DOMAIN = [
        :name_domain,
        desc: 'Domain of the servers, ex: test.com',
        aliases: ['-d']
      ].freeze

      option(*NAME_SCHEME)
      option(*NAME_DOMAIN)
      desc 'rename targets', 'rename one or more dedicated servers'
      def rename(*targets)
        servers, domain = init_rename(targets)
        # Add check on duplication?
        self.class.ask_validation(
          'You are going to rename those servers:',
          servers.list.map do |s|
            "  #{s.reverse} => #{s.newname}.#{domain.id}"
          end.join("\n")
        )
        servers.puts_each(:rename, [domain])
        puts 'To complete renaming, call "set_reverse" in a few minute'
      end

      option(*NAME_SCHEME)
      option(*NAME_DOMAIN)
      desc 'set_reverse targets', 'set the reverse for a server'
      def set_reverse(*targets) # rubocop:disable Style/AccessorMethodName
        servers, domain = init_rename(targets)
        servers.puts_each(:define_reverse, [domain])
      end

      # TODO: test it!
      desc 'ipmi target', 'create ipmi interface and launch javaws on it'
      def ipmi(*targets)
        spawner = Provisioner.init(options)
        servers = spawner.get('DedicatedServer', *self.class.all(targets))
        list = servers.list
        unless list.size == 1
          puts 'Please select one and only one target! You have targeted:'
          puts servers.format('flavor')
          exit 1
        end
        list.first.ipmi
      end

      desc 'get url', 'execute a get on url'
      def get(url)
        Provisioner.init(options)
        puts Provisioner.client.get url
      end

      desc 'vrack SUBCOMMAND ...ARGS', 'Manage Vracks'
      subcommand 'vrack', CliVrack

      desc 'domain SUBCOMMAND ...ARGS', 'Manage Domains'
      subcommand 'domain', CliDomain

      desc 'ip SUBCOMMAND ...ARGS', 'Manage IPs'
      subcommand 'ip', CliIP

      no_commands do
        def init_rename(targets)
          spawner = Provisioner.init(options)
          servers = spawner.get('DedicatedServer', *self.class.all(targets))
          name_domain = Provisioner.config['name_domain']
          domain = spawner.get('DomainZone', id: name_domain)
          [servers, domain]
        end
      end
    end
  end
end
