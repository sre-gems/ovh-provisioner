# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

vracks = {
  'pn-1111' => { 'name' => 'avrack', 'description' => '' },
  'pn-2222' => { 'name' => 'bvrack', 'description' => 'Vrack n°2' }
}

servers = {
  'pn-1111' => ['test-server-01'],
  'pn-2222' => ['test-server-02']
}

tasks = {
  321 => {
    'function' => 'removeDedicatedServerFromBridgeDomain',
    'targetDomain' => 'test-server-02',
    'status' => 'init',
    'serviceName' => 'pn-2222',
    'orderId' => nil,
    'lastUpdate' => now,
    'todoDate' => now,
    'id' => 321
  },
  322 => {
    'function' => 'addDedicatedServerToBridgeDomain',
    'targetDomain' => 'test-server-03',
    'status' => 'init',
    'serviceName' => 'pn-2222',
    'orderId' => nil,
    'lastUpdate' => now,
    'todoDate' => now,
    'id' => 322
  }
}

# Stubs
RSpec.configure do |config|
  config.before(:each) do
    url = 'https://api.test.com/1.0/vrack'

    stub_request(:get, "#{url}/")
      .to_return(status: 200, body: vracks.keys.to_json, headers: {})

    vracks.each_pair do |vrack, general|
      stub_request(:get, "#{url}/#{vrack}/")
        .to_return(status: 200, body: general.to_json, headers: {})

      serverlist = servers[vrack].to_json
      stub_request(:get, "#{url}/#{vrack}/dedicatedServer")
        .to_return(status: 200, body: serverlist, headers: {})
    end

    stub_request(:delete, "#{url}/pn-2222/dedicatedServer/test-server-02")
      .to_return(status: 200, body: tasks[321].to_json, headers: {})

    stub_request(:post, "#{url}/pn-2222/dedicatedServer")
      .with(body: '{"dedicatedServer":"test-server-03"}')
      .to_return(status: 200, body: tasks[322].to_json, headers: {})

    stub_request(:get, "#{url}/pn-1111/task")
      .to_return(status: 200, body: '[]', headers: {})

    stub_request(:get, "#{url}/pn-2222/task")
      .to_return(status: 200, body: tasks.keys.to_json, headers: {})

    tasks.each_pair do |task, body|
      stub_request(:get, "#{url}/pn-2222/task/#{task}")
        .to_return(status: 200, body: body.to_json, headers: {})
    end
  end
end
