# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'yaml'
require 'ovh/rest'

module OVH
  # Load configuration and initialize client
  module Provisioner
    class << self
      attr_reader :config
      attr_reader :client
      attr_reader :spawner

      def init(options)
        @config = load_config(options)
        @client = create_client
        @spawner = create_spawner
      end

      def load_config(options)
        config_file = options['config_file']
        begin
          config = YAML.load_file config_file if File.exist? config_file
        rescue StandardError => e
          puts "#{e}\nCould not load configuration file: #{config_file}"
          exit 1
        end
        check_missing((config || {}).merge(options))
      end

      def check_missing(config)
        missing = %w[
          app_key app_secret consumer_key api_url
        ].map { |key| config[key].nil? ? key : nil }.compact

        return config if missing.empty?

        puts "Please provide valid #{missing.join(', ')}"
        exit 1
      end

      def create_client
        return @client unless @client.nil?

        OVH::REST.new(
          config['app_key'],
          config['app_secret'],
          config['consumer_key'],
          config['api_url']
        )
      end

      def create_spawner
        return @spawner unless @spawner.nil?

        spawner = Spawner.new
        Celluloid::Actor[Spawner::NAME] = spawner
      end
    end
  end
end
