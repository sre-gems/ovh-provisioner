# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

# rubocop:disable Metrics/BlockLength
describe 'ovh-provisioner vrack', :highline do
  context 'list' do
    it 'should output the list of vracks' do
      out = <<-OUTPUT.gsub(/^  /, '')
  vrack:
    avrack: pn-1111
      dedicated_servers:
        test-server-01
    bvrack: pn-2222 - Vrack n°2
      dedicated_servers:
        test-server-02
      tasks:
        init pn-2222.add(test-server-03)
        init pn-2222.remove(test-server-02)
      OUTPUT
      expect { start(self, 'vrack') }.to output(out).to_stdout
    end
  end

  context 'rm pn-2222 server-02' do
    it 'should ask, then remove test-server-02 from vrack pn-2222' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to remove those servers from vrack pn-2222(bvrack):
  dedicated_server:
    bvrack:
      test-server-02.afirsttest.com[test-server-02/2]
        test - storage_2016 - bvrack:2.2.2.2 - rescue:centos7_64 - ok
        Rebooting
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  test-server-02: ok
    init pn-2222.remove(test-server-02)
      OUTPUT
      provide_input('yes')
      expect { start(self, 'vrack') }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end

  context 'add pn-2222 03' do
    it 'should ask, then add test-server-03 in vrack pn-2222' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to add those servers to vrack pn-2222(bvrack):
  dedicated_server:
    none:
      test-server-03.afirsttest.com[test-server-03/3]
        test - undefined - none:3.3.3.3 - pxe:centos7_64 - ok
        Server is not being installed or reinstalled at the moment
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  test-server-03: ok
    init pn-2222.add(test-server-03)
      OUTPUT
      provide_input('yes')
      expect { start(self, 'vrack') }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end
end
# rubocop:enable Metrics/BlockLength
