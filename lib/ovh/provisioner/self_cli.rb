# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'

module OVH
  module Provisioner
    # The command line runner
    class Cli < Thor
      # Exit 1 on failure
      def self.exit_on_failure?
        true
      end

      def self.ask_validation(question, what = nil)
        say question
        say what unless what.nil?
        exit unless HighLine.agree('Do you want to proceed?')
      end

      def self.check_service_input(search, services, allow_many = true)
        ok = true
        if services.empty?
          puts "No registered services of your account match #{search}"
          ok = false
        end
        if !allow_many && services.size > 1
          puts "Need one service, got many: #{services.map(&:id)}"
          ok = false
        end
        ok
      end

      def self.all(targets)
        targets.empty? ? [''] : targets
      end

      class_option(
        :config_file,
        desc: 'Configuration file to use',
        default: File.join(Dir.home, '.config', 'ovh-provisioner.yml'),
        aliases: ['-c']
      )
      class_option(
        :app_key,
        desc: 'Define/Override the Application Key',
        aliases: ['-a']
      )
      class_option(
        :app_secret,
        desc: 'Define/Override the Application Secret',
        aliases: ['-s']
      )
      class_option(
        :consumer_key,
        desc: 'Define/Override the Consumer Key',
        aliases: ['-k']
      )
      class_option(
        :api_url,
        desc: 'Override the API url',
        aliases: ['-u']
      )
    end
  end
end
