# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

# rubocop:disable Metrics/BlockLength
describe 'ovh-provisioner ip', :highline do
  context 'list' do
    it 'should output the list of IPs' do
      out = <<-OUTPUT.gsub(/^  /, '')
  ip:
    test-server-01:
      ipv4:
        1.1.1.1/32 - test-server-01(dedicated) - non endable
          reverse: test-server-01.test.com.
          description: First server
    test-server-02:
      ipv4:
        2.2.2.2/32 - test-server-02(dedicated) - endable
    test-server-03:
      ipv4:
        3.3.3.3/32 - test-server-03(dedicated) - non endable
          organisation_id: test
          country: some
      OUTPUT
      expect { start(self, 'ip') }.to output(out).to_stdout
    end
  end

  context 'set_reverse 2.2 test-server-02.test.com' do
    it 'should ask then set 2.2.2.2 reverse to test-server-02.test.com' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to set the reverse of 2.2.2.2/32 to test-server-02.test.com
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  2.2.2.2/32 - reverse test-server-02.test.com has been added
      OUTPUT
      provide_input('yes')
      expect { start(self, 'ip') }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end

  context 'set_reverse 3.3 invalid-test-03' do
    it 'should ask then failed to set 3.3.3.3 reverse to invalid-test-03' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to set the reverse of 3.3.3.3/32 to invalid-test-03
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  Cannot check if invalid-test-03. resolves to 3.3.3.3
      OUTPUT
      provide_input('yes')
      expect { start(self, 'ip') }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end

  context 'rm_reverse 1.1.1.1' do
    it 'should ask then remove 1.1.1.1 reverse' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to remove the reverse of 1.1.1.1/32
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  1.1.1.1/32 - reverse test-server-01.test.com. has been removed
      OUTPUT
      provide_input('yes')
      expect { start(self, 'ip') }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end
end
# rubocop:enable Metrics/BlockLength
