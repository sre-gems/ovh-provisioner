# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'
require 'pp'
require 'ruby-progressbar'
require 'highline/import'

module OVH
  module Provisioner
    # Command line for domain (actually domain/zone)
    class CliDomain < Thor
      # Exit 1 on failure
      def self.exit_on_failure?
        true
      end

      # options
      SUBDOMAIN = [
        :subdomain,
        default: '',
        desc: 'Record subdomain',
        aliases: ['-d']
      ].freeze

      TYPE = [
        :type,
        default: '',
        desc: 'Record type',
        aliases: ['-y']
      ].freeze

      TARGET = [
        :target,
        default: '',
        desc: 'Record target',
        aliases: ['-t']
      ].freeze

      TTL = [
        :ttl,
        default: '0',
        desc: 'Record TTL',
        aliases: ['-l']
      ].freeze

      desc 'list', 'Print the list of your domains'
      def list(*targets)
        spawner = Provisioner.init(options)
        puts spawner.get('DomainZone', *Cli.all(targets)).format
      end

      desc 'show domain', 'Show records in a domain'
      def show(domain)
        spawner = Provisioner.init(options)
        zones = spawner.get('DomainZone', domain).list
        zones.each { |z| puts z.details } if check_zone_input(domain, zones)
      end

      desc 'add domain', 'Add a record in a domain'
      [SUBDOMAIN, TYPE, TARGET, TTL].map { |o| option(*o) }
      def add(domain)
        spawner = Provisioner.init(options)
        zones = spawner.get('DomainZone', domain).list
        return unless check_zone_input(domain, zones, false)

        zone = zones.first
        add_record(zone, options)
      end

      desc 'rm domain', 'Remove records in domain'
      [SUBDOMAIN, TYPE, TARGET, TTL].map { |o| option(*o) }
      def rm(domain)
        spawner = Provisioner.init(options)
        zones = spawner.get('DomainZone', domain).list
        return unless check_zone_input(domain, zones, false)

        zone = zones.first
        matches = zone.filter_records(Provisioner.config)
        rm_records(zone, matches)
      end

      no_commands do # rubocop:disable Metrics/BlockLength
        def check_zone_input(search, zones, allow_many = true)
          ok = true
          if zones.empty?
            puts "No registered services of your account match #{search}"
            ok = false
          end
          if !allow_many && zones.size > 1
            puts "Need one zone, got many: #{zones.map(&:id)}"
            ok = false
          end
          ok
        end

        def add_record(zone, options)
          sub = options['subdomain']
          type = options['type'].upcase
          target = options['target']
          ttl = options['ttl']
          Cli.ask_validation(
            "You are going to add a record to #{zone.id}:",
            "  #{APIObject::Record.print(zone, sub, ttl, type, target)}"
          )
          puts zone.add_record(sub, type, target, ttl)
        end

        def rm_records(zone, matches)
          if matches.list.empty?
            puts 'Nothing to do…'
          else
            Cli.ask_validation(
              "You are going to remove theses zones from #{zone.id}:",
              zone.details(matches).lines[1..-1].join('')
            )
            puts zone.rm_records(matches)
          end
        end
      end
    end
  end
end
