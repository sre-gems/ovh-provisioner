# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'
require 'pp'
require 'ruby-progressbar'
require 'highline/import'

module OVH
  module Provisioner
    # The command line runner
    class CliVrack < Thor
      # Exit 1 on failure
      def self.exit_on_failure?
        true
      end

      desc 'list', 'print the list of your dedicated servers'
      def list(*targets)
        spawner = Provisioner.init(options)
        puts spawner.get('Vrack', *Cli.all(targets)).format
      end

      desc 'add vrack_id targets…', 'Add one/multiple servers in a vrack'
      def add(vrack_id, *targets)
        msg = 'You are going to add those servers to vrack'
        execute_on_vrack(vrack_id, targets, :add, msg)
      end

      desc 'rm vrack_id targets_', 'Remove one/multiple servers from a vrack'
      def rm(vrack_id, *targets)
        msg = 'You are going to remove those servers from vrack'
        execute_on_vrack(vrack_id, targets, :remove, msg)
      end

      no_commands do
        def init_vrack(vrack_id, targets)
          spawner = Provisioner.init(options)
          servers = spawner.get('DedicatedServer', *Cli.all(targets))
          vracks = spawner.get('Vrack', vrack_id)
          [servers, vracks]
        end

        def execute_on_vrack(vrack_id, targets, method, msg)
          servers, vracks = init_vrack(vrack_id, targets)
          return unless vracks.list.size == 1

          vrack = vracks.list.first
          msg = "#{msg} #{vrack.id}(#{vrack.name}):"
          Cli.ask_validation(msg, servers.format('vrack'))
          servers.puts_each(method, [], vrack)
        end
      end
    end
  end
end
