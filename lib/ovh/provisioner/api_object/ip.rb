# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

module OVH
  module Provisioner
    module APIObject
      # Represent an OVH IP
      class IP < APIObject
        attr_reader :organisation_id, :country
        attr_reader :routed_to, :can_be_terminated, :type
        attr_reader :description
        attr_reader :reverse
        attr_reader :kind

        def add_reverse(reverse)
          body = {
            'ipReverse' => id.split('/').first,
            'reverse' => reverse
          }
          response = post('reverse', body)
          unless response['reverse'].nil?
            response = "#{id} - reverse #{reverse} has been added"
            @reverse = reverse['reverse']
          end
          response
        end

        def rm_reverse
          response = delete("reverse/#{id.split('/').first}")
          response ||= "#{id} - reverse #{reverse} has been removed"
          response
        end

        def to_s
          base = "#{id} - #{routed_to}(#{type}) - #{endable}"
          %i[organisation_id country reverse description].each do |key|
            value = send(key)
            base += "\n  #{key}: #{value}" unless value.nil?
          end
          base
        end

        private

        def set_general
          general = get
          @organisation_id = general['organisationId']
          @country = general['country']
          routed = general['routedTo']
          @routed_to = routed.nil? ? nil : routed['serviceName']
          @can_be_terminated = general['canBeTerminated']
          @type = general['type']
          @description = general['description']
          @kind = id.include?(':') ? 'ipv6' : 'ipv4'
        end

        def set_reverse
          reverse = get("reverse/#{id.split('/').first}")
          @reverse = reverse['reverse']
        end

        def endable
          can_be_terminated ? 'endable' : 'non endable'
        end
      end
    end
  end
end
