# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

module OVH
  module Provisioner
    module APIObject
      # Represent a Domain
      class DomainZone < APIObject
        attr_reader :nameservers, :dnssec_supported, :dnssec, :records

        def to_s
          "#{id}, dnssec #{dnssec_to_s}, #{records.list.size} records"
        end

        def details(record_lists = records)
          "#{id} - dnssec: #{dnssec_to_s}\n"\
            "#{(record_lists.format('type').lines[1..-1] || []).join('')}"
        end

        def add_record(subdomain, type, target, ttl = 0)
          body = {
            'fieldType' => type.upcase,
            'target' => target,
            'ttl' => ttl.to_i
          }
          sub = subdomain
          body['subDomain'] = sub unless sub.nil? || sub.empty?
          answer = post('record', body)
          post('refresh')
          format(answer)
        end

        def rm_record(record)
          out = delete("record/#{record.id}")
          post('refresh')
          "  #{record}#{out.nil? ? '' : ": #{out}"}"
        end

        def rm_records(records)
          outputs = records.parallel_map(:rm_record, [], Actor.current)
          (["#{id}, remove records:"] + outputs.sort).join("\n")
        end

        def filter_records(config)
          filter = generate_filter(
            config['subdomain'] || '',
            config['type'] || '',
            config['target'] || '',
            config['ttl'] || ''
          )
          targets = records.list.dup.keep_if do |record|
            filter.call(record)
          end.map(&:id)
          Actor[Spawner::NAME].get('Record', *targets, parent: @url)
        end

        private

        def set_general
          general = get
          @nameservers = general['nameServers']
          @dnssec_supported = general['dnssecSupported']
        end

        def set_dnssec
          @dnssec = get('dnssec')['status']
        end

        def set_records
          @records = Actor[Spawner::NAME].get('Record', '', parent: @url)
        end

        def dnssec_to_s
          dnssec_supported ? dnssec : 'not supported'
        end

        def generate_filter(subdomain, type, target, ttl)
          lambda do |record|
            record.subdomain.include?(subdomain) &&
              (type.empty? || record.type == type.upcase) &&
              record.target.include?(target) &&
              (ttl == '' || record.ttl == ttl.to_i)
          end
        end

        def format(answer)
          return answer if answer.is_a? String

          zone = answer['zone']
          sub = answer['subDomain'] || ''
          ttl = answer['ttl']
          type = answer['fieldType']
          target = answer['target']
          "#{id}, add record:\n"\
            "  #{Record.print(zone, sub, ttl, type, target)}"
        end
      end
    end
  end
end
