# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

# rubocop:disable Metrics/BlockLength
subcommand = 'domain'
describe "ovh-provisioner #{subcommand}", :highline do
  context 'list' do
    it 'should output the list of domains' do
      out = <<-OUTPUT.gsub(/^  /, '')
  domain_zone:
    afirsttest.com, dnssec enabled, 6 records
    anothertest.com, dnssec disabled, 2 records
    sotested.net, dnssec not supported, 0 records
      OUTPUT
      expect { start(self, subcommand) }.to output(out).to_stdout
    end
  end

  context 'show test.com' do
    it 'should output \'afirsttest.com\' details (with records) ' do
      out = <<-OUTPUT.gsub(/^  /, '')
  afirsttest.com - dnssec: enabled
    A:
      foo 1 A 1.2.4.5
      some.sub 2 A 7.7.7.7
      test-server-01 2 A 1.1.1.1
    CNAME:
      sub.www 2 CNAME some.cname.com.
      test-undef-3 0 CNAME test-server-03
    NS:
      afirsttest.com. 0 NS somedns.ovh.net
  anothertest.com - dnssec: disabled
    MX:
      anothertest.com. 3 MX redirect.something.net
    SRV:
      _imaps._tcp 0 SRV 0 0 993 ssl.something.net
      OUTPUT
      expect { start(self, subcommand) }.to output(out).to_stdout
    end
  end

  context 'show test.org' do
    it 'should exit with error because no domains match test.org' do
      out = <<-OUTPUT.gsub(/^  /, '')
  No registered services of your account match test.org
      OUTPUT
      expect { start(self, subcommand) }.to output(out).to_stdout
    end
  end

  context 'add firsttest -d test -y cname -t serv.dom.io.' do
    it 'should add record \'test 0 CNAME serv.dom.io.\' to afirsttest.com' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to add a record to afirsttest.com:
    test 0 CNAME serv.dom.io.
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  afirsttest.com, add record:
    test 0 CNAME serv.dom.io.
      OUTPUT
      provide_input('yes')
      expect { start(self, subcommand) }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end

  context 'add firsttest -d test -y A -t serv.dom.io.' do
    it 'should fail because target (serv.dom.io.) is not a valid IPv4' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to add a record to afirsttest.com:
    test 0 A serv.dom.io.
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  Invalid IPv4 serv.dom.io. for A record of test.afirsttest.com
      OUTPUT
      provide_input('yes')
      expect { start(self, subcommand) }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end

  context 'add test.com -d test -y cname -t serv.dom.io.' do
    it 'should exit with error because two domains match test.com' do
      out = <<-OUTPUT.gsub(/^  /, '')
  Need one zone, got many: ["afirsttest.com", "anothertest.com"]
      OUTPUT
      expect { start(self, subcommand) }.to output(out).to_stdout
    end
  end

  context 'rm afirsttest -d sub -l 2' do
    it 'should ask to remove two records and do it' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to remove theses zones from afirsttest.com:
    A:
      some.sub 2 A 7.7.7.7
    CNAME:
      sub.www 2 CNAME some.cname.com.
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  afirsttest.com, remove records:
    some.sub 2 A 7.7.7.7
    sub.www 2 CNAME some.cname.com.
      OUTPUT
      provide_input('yes')
      expect { start(self, subcommand) }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end

  context 'rm afirsttest -t notarget' do
    it 'should do nothing because no records match target condition' do
      out = <<-OUTPUT.gsub(/^  /, '')
  Nothing to do…
      OUTPUT
      expect { start(self, subcommand) }.to output(out).to_stdout
    end
  end
end
# rubocop:enable Metrics/BlockLength
