# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

module OVH
  module Provisioner
    module APIObject
      # Represent a VRack
      class Vrack < APIObject
        attr_reader :name, :description, :dedicated_servers, :tasks

        def add(api_object)
          path = path_from(api_object)
          ok, msg = parse_task(post(path, path => api_object.id))
          "#{api_object.id}: #{ok ? 'ok' : 'failed'}\n  #{msg}"
        end

        def remove(api_object)
          path = "#{path_from(api_object)}/#{api_object.id}"
          ok, msg = parse_task(delete(path))
          "#{api_object.id}: #{ok ? 'ok' : 'failed'}\n  #{msg}"
        end

        def to_s
          "#{name}: #{id}#{" - #{description}" unless description.empty?}" \
          "#{list_to_s(:dedicated_servers)}" \
          "#{list_to_s(:tasks)}"
        end

        private

        def set_general
          general = get
          @name = general['name']
          @description = general['description']
        end

        def set_dedicated_server
          @dedicated_servers = get('dedicatedServer')
        end

        def set_tasks
          tasks = get('task')
          futures = tasks.map { |t| future.get("task/#{t}") }
          @tasks = futures.map do |future|
            _ok, msg = parse_task(future.value)
            msg
          end
        end

        def list_to_s(list_sym)
          list = send(list_sym)
          return '' if list.nil? || list.empty?

          header = ["\n  #{list_sym}:"]
          tail = list.map { |i| "    #{i}" }.sort
          (header + tail).join("\n")
        end

        def path_from(api_object)
          name = api_object.class.classname
          "#{name[0..0].downcase}#{name[1..-1]}"
        end

        def parse_task(msg)
          return [false, msg] if msg.is_a?(String)

          method = /^[[:lower:]]+/.match(msg['function']).to_a.first
          [
            true,
            "#{msg['status']} #{msg['serviceName']}.#{method}" \
            "(#{msg['targetDomain']})"
          ]
        end
      end
    end
  end
end
