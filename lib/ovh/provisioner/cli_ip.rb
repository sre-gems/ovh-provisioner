# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'
require 'pp'
require 'ruby-progressbar'
require 'highline/import'

module OVH
  module Provisioner
    # The command line runner
    class CliIP < Thor
      # Exit 1 on failure
      def self.exit_on_failure?
        true
      end

      desc 'list', 'print the list of your OVH IPs'
      def list(*targets)
        spawner = Provisioner.init(options)
        puts spawner.get('IP', *Cli.all(targets)).format('routed_to', 'kind')
      end

      desc 'set_reverse ip reverse', 'Set the reverse of the IP'
      def set_reverse(ip, reverse)
        spawner = Provisioner.init(options)
        ips = spawner.get('IP', ip).list
        return unless Cli.check_service_input(ip, ips, false)

        ip = ips.first
        ask = "You are going to set the reverse of #{ip.id} to #{reverse}"
        Cli.ask_validation(ask)
        puts ip.add_reverse(reverse)
      end

      desc 'rm_reverse ip', 'Remove the reverse of the IP'
      def rm_reverse(ip)
        spawner = Provisioner.init(options)
        ips = spawner.get('IP', ip).list
        return unless Cli.check_service_input(ip, ips, false)

        ip = ips.first
        ask = "You are going to remove the reverse of #{ip.id}"
        Cli.ask_validation(ask)
        puts ip.rm_reverse
      end
    end
  end
end
