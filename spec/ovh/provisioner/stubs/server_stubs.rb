# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# General config of servers
servers = (1..3).map do |i|
  {
    "test-server-0#{i}" => {
      'datacenter' => "test#{i}",
      'ip' => "#{i}.#{i}.#{i}.#{i}",
      'os' => 'centos7_64',
      'state' => 'ok',
      'serverId' => i,
      'bootId' => i,
      'name' => "test-server-0#{i}",
      'reverse' => "test-server-0#{i}.afirsttest.com."
    }
  }
end.inject(:merge)

# BootId config
boots = {
  1 => { 'bootType' => 'harddisk' },
  2 => { 'bootType' => 'rescue' },
  3 => { 'bootType' => 'pxe' }
}

# Hardware profil and flavors
hardwares = {}
%w[compute_2016 storage_2016 compute_2016].each_with_index do |flavor, i|
  config = YAML.load_file('spec/config.yml')
  hardwares["test-server-0#{i + 1}"] = config['flavors'][flavor]['hardware']
end
# We can get more info, it won't affect the flavor
hardwares['test-server-02']['motherboard'] = 'X10DRH-iT'
# If a value is different, it is a different flavor
hardwares['test-server-03']['memorySize']['value'] = 262_144

# Vracks
vracks = {
  'test-server-01' => ['pn-1111'],
  'test-server-02' => ['pn-2222'],
  'test-server-03' => []
}

# Install status
idle = OVH::Provisioner::APIObject::DedicatedServer::IDLE
install_status = {
  'test-server-01' => [
    200,
    '{ "progress": [
        {"status": "doing", "comment": "Setting things up"},
        {"status": "to do", "comment": "Rebooting on fresh system"}
      ]
    }'
  ],
  'test-server-02' => [
    200,
    '{ "progress": [
        {"status": "done", "comment": "Preparing installation"},
        {"status": "doing", "comment": "Rebooting"}
      ]
    }'
  ],
  'test-server-03' => [
    404, "{\"message\": \"#{idle}\"}"
  ]
}

def install_body(server, distrib = nil, ssh = 'testKey')
  details = { 'customHostname' => "#{server}.afirsttest.com" }
  details['useDistribKernel'] = distrib unless distrib.nil?
  details['sshKeyName'] = ssh unless ssh.nil?
  {
    'templateName' => 'test-template',
    'details' => details
  }
end

reinstall_answer = <<-JSON.gsub(/^  /, '')
  {
    "taskId": 123,
    "function": "reinstallServer",
    "lastUpdate": "#{now}",
    "comment": "Start dedicated server installation",
    "status": "init",
    "startDate": "#{now}",
    "doneDate": null
  }
JSON

# Stubs
# rubocop:disable Metrics/BlockLength
RSpec.configure do |config|
  config.before(:each) do
    url = 'https://api.test.com/1.0/dedicated/server'

    stub_request(:get, "#{url}/")
      .to_return(status: 200, body: servers.keys.to_json, headers: {})

    servers.each_pair do |server, general|
      stub_request(:get, "#{url}/#{server}/")
        .to_return(status: 200, body: general.to_json, headers: {})

      boot_id = general['bootId']
      boot = boots[boot_id].to_json
      stub_request(:get, "#{url}/#{server}/boot/#{boot_id}")
        .to_return(status: 200, body: boot, headers: {})

      hardware = hardwares[server].to_json
      stub_request(:get, "#{url}/#{server}/specifications/hardware")
        .to_return(status: 200, body: hardware, headers: {})

      vrack = vracks[server].to_json
      stub_request(:get, "#{url}/#{server}/vrack")
        .to_return(status: 200, body: vrack, headers: {})

      status, body = install_status[server]
      stub_request(:get, "#{url}/#{server}/install/status")
        .to_return(status: status, body: body, headers: {})

      stub_request(:post, "#{url}/#{server}/install/start")
        .with(body: install_body(server, false).to_json)
        .to_return(status: 200, body: reinstall_answer, headers: {})

      stub_request(:post, "#{url}/#{server}/install/start")
        .with(body: install_body(server, true).to_json)
        .to_return(status: 200, body: reinstall_answer, headers: {})
    end
  end
end
# rubocop:enable Metrics/BlockLength
