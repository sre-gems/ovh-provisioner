# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

module OVH
  module Provisioner
    module APIObject
      # Represent a record in a domain zone
      class Record < APIObject
        attr_reader :target, :ttl, :zone, :type, :subdomain

        def self.print(zone, subdomain, ttl, type, target)
          "#{subdomain.empty? ? "#{zone}." : subdomain} "\
            "#{ttl} #{type} #{target}".strip
        end

        def to_s
          self.class.print(zone, subdomain, ttl, type, target)
        end

        private

        def set_general
          general = get
          @target = general['target']
          @ttl = general['ttl']
          @zone = general['zone']
          @type = general['fieldType']
          @subdomain = general['subDomain']
        end
      end
    end
  end
end
