# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

domains = {
  'afirsttest.com' => {
    'lastUpdate' => now,
    'hasDnsAnycast' => false,
    'nameServers' => ['somedns.ovh.net', 'somens.ovh.net'],
    'dnssecSupported' => true
  },
  'anothertest.com' => {
    'lastUpdate' => now,
    'hasDnsAnycast' => false,
    'nameServers' => ['somedns.ovh.net', 'somens.ovh.net'],
    'dnssecSupported' => true
  },
  'sotested.net' => {
    'lastUpdate' => now,
    'hasDnsAnycast' => false,
    'nameServers' => ['somedns.ovh.net', 'somens.ovh.net'],
    'dnssecSupported' => false
  }
}

dnssecs = {
  'afirsttest.com' => { 'status' => 'enabled' },
  'anothertest.com' => { 'status' => 'disabled' },
  'sotested.net' => { 'status' => 'disabled' }
}

records = {
  'afirsttest.com' => [1, 2, 4, 7, 9, 10],
  'anothertest.com' => [3, 5],
  'sotested.net' => []
}

infos = {
  1 => {
    'target' => 'somedns.ovh.net',
    'ttl' => 0,
    'zone' => 'afirsttest.com',
    'fieldType' => 'NS',
    'id' => 1,
    'subDomain' => ''
  },
  2 => {
    'target' => '1.2.4.5',
    'ttl' => 1,
    'zone' => 'afirsttest.com',
    'fieldType' => 'A',
    'id' => 2,
    'subDomain' => 'foo'
  },
  4 => {
    'target' => 'some.cname.com.',
    'ttl' => 2,
    'zone' => 'afirsttest.com',
    'fieldType' => 'CNAME',
    'id' => 4,
    'subDomain' => 'sub.www'
  },
  3 => {
    'target' => 'redirect.something.net',
    'ttl' => 3,
    'zone' => 'anothertest.com',
    'fieldType' => 'MX',
    'id' => 3,
    'subDomain' => ''
  },
  5 => {
    'target' => '0 0 993 ssl.something.net',
    'ttl' => 0,
    'zone' => 'anothertest.com',
    'fieldType' => 'SRV',
    'id' => 5,
    'subDomain' => '_imaps._tcp'
  },
  7 => {
    'target' => '7.7.7.7',
    'ttl' => 2,
    'zone' => 'afirsttest.com',
    'fieldType' => 'A',
    'id' => 7,
    'subDomain' => 'some.sub'
  },
  9 => { # Will be deleted by rename
    'target' => '1.1.1.1',
    'ttl' => 2,
    'zone' => 'afirsttest.com',
    'fieldType' => 'A',
    'id' => 9,
    'subDomain' => 'test-server-01'
  },
  10 => { # use by rename, refuse 03 renaming
    'target' => 'test-server-03',
    'ttl' => 0,
    'zone' => 'afirsttest.com',
    'fieldType' => 'CNAME',
    'id' => 10,
    'subDomain' => 'test-undef-3'
  }
}

body_add_cname = {
  'fieldType' => 'CNAME',
  'target' => 'serv.dom.io.',
  'ttl' => 0,
  'subDomain' => 'test'
}

body_add_a = body_add_cname.dup
body_add_a['fieldType'] = 'A'

result_add_cname = body_add_cname.dup.merge(
  'zone' => 'afirsttest.com',
  'id' => 6
)

result_add_a = {
  'message' => "Invalid IPv4 #{body_add_a['target']} for "\
  "#{body_add_a['fieldType']} record of "\
  "#{body_add_a['subDomain']}.#{result_add_cname['zone']}"
}

def rename_body(xxx)
  {
    'fieldType' => 'A',
    'target' => "#{xxx}.#{xxx}.#{xxx}.#{xxx}",
    'ttl' => 0,
    'subDomain' => "test-#{xxx == 1 ? 'comp' : 'sto'}16-#{xxx}"
  }
end

def rename_result(xxx)
  rename_body(xxx).dup.merge(
    'zone' => 'afirsttest.com',
    'id' => 8
  )
end

# Stubs
# rubocop:disable Metrics/BlockLength
RSpec.configure do |config|
  config.before(:each) do
    url = 'https://api.test.com/1.0/domain/zone'

    stub_request(:get, "#{url}/")
      .to_return(status: 200, body: domains.keys.to_json, headers: {})

    domains.each_pair do |domain, general|
      stub_request(:get, "#{url}/#{domain}/")
        .to_return(status: 200, body: general.to_json, headers: {})

      stub_request(:get, "#{url}/#{domain}/dnssec")
        .to_return(status: 200, body: dnssecs[domain].to_json, headers: {})

      stub_request(:get, "#{url}/#{domain}/record/")
        .to_return(status: 200, body: records[domain].to_json, headers: {})

      records[domain].each do |record|
        stub_request(:get, "#{url}/#{domain}/record/#{record}/")
          .to_return(status: 200, body: infos[record].to_json, headers: {})
      end
    end

    stub_request(:post, "#{url}/afirsttest.com/record")
      .with(body: body_add_cname.to_json)
      .to_return(status: 200, body: result_add_cname.to_json, headers: {})

    stub_request(:post, "#{url}/afirsttest.com/record")
      .with(body: body_add_a.to_json)
      .to_return(status: 400, body: result_add_a.to_json, headers: {})

    stub_request(:post, "#{url}/afirsttest.com/refresh")
      .to_return(status: 200, body: 'null', headers: {})

    %w[4 7 9].each do |id|
      stub_request(:delete, "#{url}/afirsttest.com/record/#{id}")
        .to_return(status: 200, body: 'null', headers: {})
    end

    (1..2).each do |x|
      stub_request(:post, "#{url}/afirsttest.com/record")
        .with(body: rename_body(x).to_json)
        .to_return(status: 200, body: rename_result(x).to_json, headers: {})
    end
  end
end
# rubocop:enable Metrics/BlockLength
