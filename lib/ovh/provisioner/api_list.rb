# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'celluloid/current'
require 'yaml'

# rubocop:disable Metrics/ClassLength
module OVH
  module Provisioner
    # Represent an API list, that is a collection of a given kind of object
    class APIList
      include Celluloid
      attr_reader :list

      # targets is a list of regex on object id
      # to match nothing, do not set any target
      # to match everything, add a '' target
      def initialize(api_object, parent, *targets)
        @parent = parent
        @url = api_object.entrypoint(parent).to_s
        @name = api_object.name.split('::').last
        @filter = generate_filter(targets)
        @futures = {}
        @list = []
      end

      def init_properties
        write = !@completed
        unless @completed
          # In ATOM concurrency, this is enough to avoid a race condition.
          @completed = true
          spawner = Actor[Spawner::NAME]
          get.keep_if { |s| @filter.call(s) }.each do |i|
            @futures[i] = spawner.future.get(@name, parent: @parent, id: i)
          end
        end
        wait(write)
      end

      def format(*fields)
        items = classify(list, *fields)
        items = deep_sort(items)
        yamlize(items)
      end

      def parallel_map(method, args = [], subject = nil)
        futures =
          if subject.nil?
            list.map { |item| item.future.send(method, *args) }
          else
            list.map { |item| subject.future.send(method, item, *args) }
          end
        futures.map(&:value)
      end

      def puts_each(method, args = [], subject = nil, remove_duplicate = true)
        results = parallel_map(method, args, subject)
        results = results.map(&:lines).flatten.compact.uniq if remove_duplicate
        results.each { |i| puts i unless i.nil? }
      end

      private

      def get(path = '')
        OVH::Provisioner.client.get("#{@url}/#{path}")
      end

      def wait(write = false)
        @futures.each_pair do |key, future|
          v = future.value
          @list << v if write
          @futures.delete(key)
        end
        @list.sort! if write
      end

      def generate_filter(targets)
        lambda do |id|
          targets.reduce(false) do |acc, target|
            acc | (id.to_s =~ /#{target.to_s}/)
          end
        end
      end

      def classify(items, *fields)
        items.each_with_object({}) do |item, hash|
          item = organize(item, *fields)
          insert!(hash, @url[1..-1].tr('/', '_') => item)
          hash
        end
      end

      def organize(item, *fields)
        head, *tail = *fields
        return [item.to_s] if head.nil?

        key = item.send(head.to_sym)
        { key => organize(item, *tail) }
      end

      def insert!(hash, item)
        if hash.is_a?(Array)
          hash << item.first
        else
          key = item.keys.first
          if hash[key].nil?
            hash.merge!(item)
          else
            insert!(hash[key], item[key])
          end
        end
      end

      def deep_sort(hash)
        return hash.sort if hash.is_a?(Array)
        return hash unless hash.is_a?(Hash)

        result = {}
        hash.each { |k, v| result[k] = deep_sort(v) }
        result.sort.to_h
      end

      def yamlize(items)
        token = 'TO_REMOVE_AFTER_TOYAML'
        yaml = prep_for_yaml(items, token).to_yaml
        yaml.lines[1..-1].grep_v(/\- \|\-/).grep_v(/#{token}/).join('')
      end

      def prep_for_yaml(obj, token)
        if obj.is_a?(Hash)
          obj.map { |k, v| [k, prep_for_yaml(v, token)] }.to_h
        elsif obj.is_a?(Array)
          obj.map { |e| prep_for_yaml(e, token) }
        elsif obj.is_a?(String)
          "#{obj}\n#{token}"
        else
          obj
        end
      end
    end
  end
end
# rubocop:enable Metrics/ClassLength
