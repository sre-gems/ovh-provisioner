# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ips = (1..3).map do |i|
  {
    "#{i}.#{i}.#{i}.#{i}" => {
      'organisationId' => i == 3 ? 'test' : nil,
      'country' => i == 3 ? 'some' : nil,
      'routedTo' => {
        'serviceName' => "test-server-0#{i}"
      },
      'ip' => "#{i}.#{i}.#{i}.#{i}/32",
      'canBeTerminated' => i == 2,
      'type' => 'dedicated',
      'description' => i == 1 ? 'First server' : nil
    }
  }
end.inject(:merge)

reverse = {
  '1.1.1.1' => [
    200,
    {
      'ipReverse' => '1.1.1.1',
      'reverse' => 'test-server-01.test.com.'
    }
  ],
  '2.2.2.2' => [
    404,
    {
      'message' => 'The requested object (ipReverse = 2.2.2.2) does not exist'
    }
  ],
  '3.3.3.3' => [
    404,
    {
      'message' => 'The requested object (ipReverse = 3.3.3.3) does not exist'
    }
  ]
}

set2 = {
  'ipReverse' => '2.2.2.2',
  'reverse' => 'test-server-02.test.com'
}

ret2 = set2.dup
ret2[reverse] = "#{ret2[reverse]}."

set1 = {
  'ipReverse' => '1.1.1.1',
  'reverse' => 'test-comp16-1.afirsttest.com'
}

fail1 = {
  'message' =>
  'Cannot check if test-comp16-1.afirsttest.com. resolves to 1.1.1.1'
}

set22 = {
  'ipReverse' => '2.2.2.2',
  'reverse' => 'test-sto16-2.afirsttest.com'
}

fail22 = {
  'message' =>
  'Cannot check if test-sto16-2.afirsttest.com. resolves to 2.2.2.2'
}

set3 = {
  'ipReverse' => '3.3.3.3',
  'reverse' => 'invalid-test-03'
}

fail3 = {
  'message' => 'Cannot check if invalid-test-03. resolves to 3.3.3.3'
}

set32 = {
  'ipReverse' => '3.3.3.3',
  'reverse' => 'test-undef-3.afirsttest.com'
}

fail32 = {
  'message' => "Cannot check if #{set32['reverse']}. resolves to 3.3.3.3"
}

ret32 = set32.dup.merge('reverse' => "#{set32['reverse']}.")

# Stubs
# rubocop:disable Metrics/BlockLength
RSpec.configure do |config|
  config.before(:each) do
    url = 'https://api.test.com/1.0/ip'

    stub_request(:get, "#{url}/")
      .to_return(status: 200,
                 body: ips.keys.map { |ip| ips[ip]['ip'] }.to_json,
                 headers: {})

    ips.each_pair do |ip, general|
      ip_url = "#{ip}%2F32"
      stub_request(:get, "#{url}/#{ip_url}/")
        .to_return(status: 200, body: general.to_json, headers: {})

      stub_request(:get, "#{url}/#{ip_url}/reverse/#{ip}")
        .to_return(status: reverse[ip].first,
                   body: reverse[ip][-1].to_json,
                   headers: {})
    end

    stub_request(:post, "#{url}/2.2.2.2%2F32/reverse")
      .with(body: set2.to_json)
      .to_return(status: 200, body: ret2.to_json, headers: {})

    stub_request(:post, "#{url}/1.1.1.1%2F32/reverse")
      .with(body: set1.to_json)
      .to_return(status: 400, body: fail1.to_json, headers: {})

    stub_request(:post, "#{url}/2.2.2.2%2F32/reverse")
      .with(body: set22.to_json)
      .to_return(status: 400, body: fail22.to_json, headers: {})

    stub_request(:post, "#{url}/3.3.3.3%2F32/reverse")
      .with(body: set3.to_json)
      .to_return(status: 400, body: fail3.to_json, headers: {})

    stub_request(:delete, "#{url}/1.1.1.1%2F32/reverse/1.1.1.1")
      .to_return(status: 200, body: nil.to_json, headers: {})

    stub_request(:post, "#{url}/3.3.3.3%2F32/reverse")
      .with(body: set32.to_json)
      .to_return(status: 400, body: fail32.to_json, headers: {})
      .to_return(status: 200, body: ret32.to_json, headers: {})
  end
end
# rubocop:enable Metrics/BlockLength
