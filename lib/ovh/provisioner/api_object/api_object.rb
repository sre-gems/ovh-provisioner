# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'celluloid/current'

module OVH
  module Provisioner
    module APIObject
      # Base API Object
      class APIObject
        include Celluloid
        include Comparable

        # Define @attributes to contain all attr_readers
        def self.attr_reader(*vars)
          @attributes ||= superclass.attributes.dup unless superclass == Object
          @attributes ||= []
          @attributes.concat vars
          super
        end

        attr_reader :id

        def self.attributes # rubocop:disable Style/TrivialAccessors
          @attributes
        end

        def attributes
          self.class.attributes
        end

        def initialize(id, parent = nil)
          @id = id
          @url = "#{self.class.entrypoint(parent)}/#{normalize(id)}"
          @futures = {}
        end

        def init_properties
          unless @completed
            # In ATOM concurrency, this is enough to avoid a race condition.
            @completed = true
            infos = private_methods(false).grep(/^set_/)
            infos.each { |i| @futures[i] = future.send(i) }
          end
          @futures.each_pair do |key, future|
            future.value
            @futures.delete(key)
          end
        end

        def to_s
          "#{self.class.entrypoint}: #{id}\n" \
          "#{attributes.map { |s| "- #{send(s)}" }.join("\n")}"
        end

        def self.entrypoint(parent = nil)
          classpath = underscore(classname).tr('_', '/')
          parent.nil? ? "/#{classpath}" : "#{parent}/#{classpath}"
        end

        def self.classname
          name.split('::').last
        end

        # inspired from Rails' ActiveSupport
        def self.underscore(string)
          res = string.gsub(/::/, '/')
          res.gsub!(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
          res.gsub!(/([a-z\d])([A-Z])/, '\1_\2')
          res.tr('-', '_').downcase
        end

        def <=>(other)
          id <=> other.id
        end

        def config
          OVH::Provisioner.config
        end

        def get(path = '')
          call(:get, ["#{@url}/#{path}"])
        end

        def post(path, body = nil)
          call(:post, ["#{@url}/#{path}", body])
        end

        def delete(path = '')
          call(:delete, ["#{@url}/#{path}"])
        end

        private

        def call(method, args)
          OVH::Provisioner.client.send(method, *args)
        rescue OVH::RESTError => error
          error.to_s.split(':')[-1].strip
        end

        def normalize(id)
          id = id.gsub('/', '%2F') if id.is_a? String
          id
        end
      end
    end
  end
end

Dir[File.dirname(__FILE__) + '/*.rb'].each { |file| require file }
