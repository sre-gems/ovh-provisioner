# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'hashdiff'

module OVH
  module Provisioner
    module APIObject
      # Represent a dedicated server
      class DedicatedServer < APIObject # rubocop:disable Metrics/ClassLength
        # install_status const
        IDLE = 'Server is not being installed or reinstalled at the moment'
        NO_OS = 'none_64'

        attr_reader :server_id, :reverse, :ip, :dc, :location, :os, :boot
        attr_reader :state
        attr_reader :flavor, :flavor_tag
        attr_reader :vrack, :vrack_id
        attr_reader :install_status

        def install
          raise 'Please provide a valid template' if config['template'].nil?

          ok, reason = can_install
          if ok
            result = post('install/start', install_body)
            if result.is_a?(String) # Meaning an error message
              reason = result
              ok = false
            end
          end
          "#{id}: #{ok ? 'ok' : 'failed'}\n  #{reason}"
        end

        def to_s
          "#{reverse}[#{id}/#{server_id}]" \
          "\n  #{location} - #{flavor} - #{vrack}:#{ip} " \
          "- #{boot}:#{os} - #{state}" \
          "\n  #{install_status}"
        end

        def newname
          name_scheme = Provisioner.config['name_scheme']
          return reverse if name_scheme.nil?

          subst = attributes.map { |a| { a => send(a) } }.reduce(&:merge)
          name_scheme % subst
        end

        def rename(domain, name = newname)
          full_name = "#{name}.#{domain.id}"
          return "#{reverse}: no change" if reverse == full_name

          # check if name is already used
          records = domain.filter_records('subdomain' => newname)
          return "#{reverse}: name already used" unless records.list.empty?

          # remove old name (only if it belongs to the same domain)
          remove_old_names(domain)

          domain.add_record(name, 'A', ip)
        end

        def define_reverse(domain, name = newname)
          full_name = "#{name}.#{domain.id}"
          return "#{reverse}: no change" if reverse == full_name

          add_reverse(full_name)
        end

        def ipmi
          request_ipmi

          (1..10).each do |_|
            result = get('features/ipmi/access?type=kvmipJnlp')
            unless result.is_a?(String)
              launch_ipmi(result['value'])
              break
            end
            sleep(1)
          end
        end

        private

        def can_install
          if install_status != IDLE
            [false, "Installation ongoing: #{install_status}"]
          elsif !config['force'] && os != NO_OS
            [false,
             "An OS is already installed (#{os}) but option force is false"]
          else [true, "Installation of #{config['template']} launched"]
          end
        end

        def install_body
          details = { 'customHostname' => reverse.chomp('.') }
          %w[customHostname useDistribKernel sshKeyName].each do |opt|
            value = config[APIObject.underscore(opt)]
            details.merge!(opt => value) unless value.nil?
          end
          {
            'templateName' => config['template'],
            'details' => details
          }
        end

        def set_general
          general = get
          @reverse = get_reverse(general)
          @server_id = general['serverId']
          @ip = general['ip']
          @dc = general['datacenter']
          @location = dc.gsub(/[0-9]*$/, '')
          @os = general['os']
          @boot = get_boot(general)
          @state = general['state']
        end

        def get_reverse(general)
          (general['reverse'] || id).chomp('.')
        end

        def get_boot(general)
          get("boot/#{general['bootId']}")['bootType']
        end

        def set_flavor
          hardware = get('specifications/hardware')
          flavors = get_flavors_from_config(hardware)
          raise "Too many flavor possible: #{flavors}" if flavors.size > 1

          @flavor, @flavor_tag =
            if flavors.first.nil?
              %w[undefined undef]
            else
              [flavors.first, config['flavors'][flavors.first]['tag']]
            end
        end

        def get_flavors_from_config(hardware)
          (config['flavors'].dup || {}).map do |flavor, desc|
            diff = HashDiff.diff(hardware, desc['hardware'])
            signs = diff.map(&:first).uniq - ['-']
            signs.empty? ? flavor : nil
          end.compact
        end

        def set_vrack
          vracks = get('vrack')
          raise "Too many vracks for #{server}: #{vracks}" if vracks.size > 1

          if vracks.first.nil?
            @vrack_id = '-1'
            @vrack = 'none'
          else
            @vrack_id = vracks.first
            @vrack = Actor[Spawner::NAME].get('Vrack', id: @vrack_id).name
          end
        end

        def set_install_status
          status = get('install/status')
          if status.is_a?(String)
            status = {
              'progress' => ['status' => 'doing', 'comment' => status]
            }
          end
          @install_status = extract_status(status)
        end

        def extract_status(status)
          cur = status['progress'].select { |step| step['status'] == 'doing' }
          cur << { 'comment' => 'Preparing installation' } if cur.empty?
          cur.first['comment']
        end

        def remove_old_names(domain)
          old_name = reverse.match(/^(.*)\.#{domain.id}$/).to_a.last
          return if old_name.nil?

          records = domain.filter_records('subdomain' => old_name)
          records.parallel_map(:rm_record, [], domain)
        end

        def add_reverse(full_name)
          ip_obj = Actor[Spawner::NAME].get('IP', id: "#{ip}/32")
          result = ip_obj.add_reverse(full_name)
          cant = "Cannot check if #{full_name}. resolves to #{ip}"
          if result == cant
            result = "#{reverse}: DNS propagation is not finished, try later_"
          end
          result
        end

        def request_ipmi
          body = { 'ttl' => '15', 'type' => 'kvmipJnlp' }
          result = post('features/ipmi/access', body)
          puts result if result.is_a?(String)
        end

        def launch_ipmi(jnlp)
          filename = "/tmp/#{id}.jnlp"
          File.open(filename, 'w') { |file| file.write(jnlp) }
          system("javaws #{filename}")
        end
      end
    end
  end
end
