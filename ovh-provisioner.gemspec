# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ovh/provisioner/version'

Gem::Specification.new do |spec|
  spec.name          = 'ovh-provisioner'
  spec.version       = OVH::Provisioner::VERSION
  spec.authors       = ['Samuel Bernard']
  spec.email         = ['samuel.bernard@gmail.com']
  spec.license       = 'Apache-2.0'

  spec.summary       = 'Provision dedicated servers hosted by OVH'
  spec.description   = IO.read(File.join(File.dirname(__FILE__), 'README.md'))
  spec.homepage      = 'https://gitlab.com/sre-gems/ovh-provisioner'

  spec.files         = `git ls-files`.lines.map(&:chomp)
  spec.bindir        = 'bin'
  spec.executables   = `git ls-files bin/*`.lines.map do |exe|
    File.basename(exe.chomp)
  end
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'rake', '~> 12'
  spec.add_development_dependency 'rspec', '~> 3'
  spec.add_development_dependency 'rubocop', '~> 0.40'
  spec.add_development_dependency 'webmock', '~> 3'

  spec.add_dependency 'celluloid', '~> 0.17'
  spec.add_dependency 'hashdiff', '~> 0.3'
  spec.add_dependency 'highline', '~> 2.0'
  spec.add_dependency 'ovh-rest', '~> 0.0.5'
  spec.add_dependency 'ruby-progressbar', '~> 1.10'
  spec.add_dependency 'thor', '~> 0.19'
end
