# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'celluloid/current'
require 'ovh/provisioner/api_object/api_object'

module OVH
  module Provisioner
    # Is responsible for spawning other cells
    class Spawner
      include Celluloid

      NAME = 'Spawner'

      # Return and create on demand a given api_object or api_list
      #
      # - class_name is an api_object class name
      # - args is an array of arguments used to object creation
      # - parent is the requester (like a vrack asking for its tasks)
      # - id is the id of the api_object (nil for api_list)
      #
      # Example:
      # - get('Vrack', id: 'pn-123'): Vrack.new('pn-123')
      # - get('Task', parent: 'vrack/pn-12', id: '98'):
      #     Task('98', 'vrack/pn-12')
      # - get('DedicatedServer', '03'): APIList.new(DedicatedServer, '03')
      def get(class_name, *args, parent: nil, id: nil)
        cell_name = "#{parent}:#{class_name}@#{id}##{args}"
        cell = Actor[cell_name.to_sym] ||= create(class_name, parent, id, args)
        cell.init_properties
        cell
      end

      private

      def create(class_name, parent, id, args)
        exclusive do
          cell_class = APIObject.const_get(class_name.to_sym)
          if id.nil?
            APIList.new(cell_class, parent, *args)
          else
            cell_class.new(id, parent, *args)
          end
        end
      end
    end
  end
end
