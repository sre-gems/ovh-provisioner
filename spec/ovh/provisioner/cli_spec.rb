# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
# rubocop:disable Metrics/BlockLength
describe 'ovh-provisioner', :highline do
  context 'version' do
    it 'should output the correct version' do
      out = "OVH Provisioner version #{OVH::Provisioner::VERSION}\n"
      expect { start(self) }.to output(out).to_stdout
    end
  end

  context 'list' do
    it 'should output the list of dedicated servers' do
      out = <<-OUTPUT.gsub(/^  /, '')
  dedicated_server:
    avrack:
      compute_2016:
        test-server-01.afirsttest.com[test-server-01/1]
          test - compute_2016 - avrack:1.1.1.1 - harddisk:centos7_64 - ok
          Setting things up
    bvrack:
      storage_2016:
        test-server-02.afirsttest.com[test-server-02/2]
          test - storage_2016 - bvrack:2.2.2.2 - rescue:centos7_64 - ok
          Rebooting
    none:
      undefined:
        test-server-03.afirsttest.com[test-server-03/3]
          test - undefined - none:3.3.3.3 - pxe:centos7_64 - ok
          Server is not being installed or reinstalled at the moment
      OUTPUT
      expect { start(self) }.to output(out).to_stdout
    end
  end

  context 'install 03' do
    it 'should ask but then refuse to reinstall test-server-03' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to (re)install those servers:
  dedicated_server:
    Server is not being installed or reinstalled at the moment:
      undefined:
        test-server-03.afirsttest.com[test-server-03/3]
          test - undefined - none:3.3.3.3 - pxe:centos7_64 - ok
          Server is not being installed or reinstalled at the moment
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  test-server-03: failed
    An OS is already installed (centos7_64) but option force is false
      OUTPUT
      provide_input('yes')
      expect { start(self) }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end

  context 'install --force 03' do
    it 'should reinstall test-server-03 with distrib kernel' do
      out = <<-OUTPUT.gsub(/^  /, '')
  test-server-03: ok
    Installation of test-template launched
      OUTPUT
      provide_input('yes')
      expect { start(self) }.to output(out).to_stdout
      url = 'https://api.test.com/1.0/dedicated/server/test-server-03'
      body = {
        'templateName' => 'test-template',
        'details' => {
          'customHostname' => 'test-server-03.afirsttest.com',
          'useDistribKernel' => true,
          'sshKeyName' => 'testKey'
        }
      }
      expect(a_request(:post, "#{url}/install/start").with(body: body))
        .to have_been_made.times(1)
    end
  end

  context 'install --no-use-distrib-kernel --force 03' do
    it 'should reinstall test-server-03 with ovh kernel' do
      out = <<-OUTPUT.gsub(/^  /, '')
  test-server-03: ok
    Installation of test-template launched
      OUTPUT
      provide_input('yes')
      expect { start(self) }.to output(out).to_stdout
      url = 'https://api.test.com/1.0/dedicated/server/test-server-03'
      body = {
        'templateName' => 'test-template',
        'details' => {
          'customHostname' => 'test-server-03.afirsttest.com',
          'useDistribKernel' => false,
          'sshKeyName' => 'testKey'
        }
      }
      expect(a_request(:post, "#{url}/install/start").with(body: body))
        .to have_been_made.times(1)
    end
  end

  name_scheme = '%<location>s-%<flavor_tag>s-%<server_id>s'
  context "rename 01 02 -n #{name_scheme} -d afirsttest.com" do
    it 'should ask then rename test-server-0[1, 2] while removing old names' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to rename those servers:
    test-server-01.afirsttest.com => test-comp16-1.afirsttest.com
    test-server-02.afirsttest.com => test-sto16-2.afirsttest.com
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  afirsttest.com, add record:
    test-comp16-1 0 A 1.1.1.1
    test-sto16-2 0 A 2.2.2.2
  To complete renaming, call "set_reverse" in a few minute
      OUTPUT
      provide_input('yes')
      expect { start(self) }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
      url = 'https://api.test.com/1.0/domain/zone'
      expect(a_request(:delete, "#{url}/afirsttest.com/record/9"))
        .to have_been_made.times(1)
    end
  end

  context "rename 03 -n #{name_scheme} -d afirsttest.com" do
    it 'should ask but failed to rename test-server-03 (name already used)' do
      ask = <<-OUTPUT.gsub(/^  /, '')
  You are going to rename those servers:
    test-server-03.afirsttest.com => test-undef-3.afirsttest.com
  Do you want to proceed?
      OUTPUT
      out = <<-OUTPUT.gsub(/^  /, '')
  test-server-03.afirsttest.com: name already used
  To complete renaming, call "set_reverse" in a few minute
      OUTPUT
      provide_input('yes')
      expect { start(self) }.to output(out).to_stdout
      expect(consume_output).to eq(ask)
    end
  end

  context "set_reverse 01 02 -n #{name_scheme} -d afirsttest.com" do
    it 'should fail to set_reverse test-server-0[1, 2] (does not resolve)' do
      out = <<-OUTPUT.gsub(/^  /, '')
  test-server-01.afirsttest.com: DNS propagation is not finished, try later_
  test-server-02.afirsttest.com: DNS propagation is not finished, try later_
      OUTPUT
      expect { start(self) }.to output(out).to_stdout
    end
  end

  context "set_reverse 03 -n #{name_scheme} -d afirsttest.com" do
    it 'should set the reverse of test-server-03 in two tries' do
      out1 = <<-OUTPUT.gsub(/^  /, '')
  test-server-03.afirsttest.com: DNS propagation is not finished, try later_
      OUTPUT
      out2 = <<-OUTPUT.gsub(/^  /, '')
  3.3.3.3/32 - reverse test-undef-3.afirsttest.com has been added
      OUTPUT
      expect { start(self) }.to output(out1).to_stdout
      expect { start(self) }.to output(out2).to_stdout
      url = 'https://api.test.com/1.0/ip'
      expect(a_request(:post, "#{url}/3.3.3.3%2F32/reverse"))
        .to have_been_made.times(2)
    end
  end
end
# rubocop:enable Metrics/BlockLength
